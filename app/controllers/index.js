$.index.open();

// var models = Alloy.Collections.model;	// singleton
var models = $.models;

var model = Alloy.createModel('model', { name: 'gus', image: '/gus.JPG' });
models.push(model);

model = Alloy.createModel('model', { name: 'astros', image: '/astros.jpg' });
models.add(model);

model = Alloy.createModel('model', { name: 'finterns', image: '/finterns.png' });
models.add(model);

model = Alloy.createModel('model', { name: 'guns', image: '/guns.jpg' });
models.add(model);

model = Alloy.createModel('model', { name: 'rockets', image: '/rockets.jpg' });
models.add(model);

$.index.addEventListener('close', function() {
    $.destroy();
});