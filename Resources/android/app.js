var Alloy = require("alloy"), _ = Alloy._, Backbone = Alloy.Backbone;

Alloy.Models.model = Alloy.Models.instance("model");

Alloy.Models.model.set("name", "model name");

Alloy.Collections.model = Alloy.Collections.instance("model");

Alloy.createController("index");