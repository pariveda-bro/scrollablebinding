function Controller() {
    function __alloyId12(e) {
        if (e && e.fromAdapter) return;
        __alloyId12.opts || {};
        var models = __alloyId11.models;
        var len = models.length;
        var views = [];
        for (var i = 0; len > i; i++) {
            var __alloyId2 = models[i];
            __alloyId2.__transform = {};
            var __alloyId4 = Ti.UI.createView({
                views: __alloyId1,
                showPagingControl: "true",
                height: "200"
            });
            views.push(__alloyId4);
            var __alloyId6 = Ti.UI.createImageView({
                image: "undefined" != typeof __alloyId2.__transform["image"] ? __alloyId2.__transform["image"] : __alloyId2.get("image")
            });
            __alloyId4.add(__alloyId6);
            var __alloyId8 = Ti.UI.createView({
                backgroundColor: "black",
                opacity: "0.6",
                height: "15%",
                top: "0"
            });
            __alloyId4.add(__alloyId8);
            var __alloyId10 = Ti.UI.createLabel({
                width: Ti.UI.SIZE,
                height: Ti.UI.SIZE,
                color: "white",
                text: "undefined" != typeof __alloyId2.__transform["name"] ? __alloyId2.__transform["name"] : __alloyId2.get("name"),
                top: "0",
                left: "10dip",
                horizontalWrap: "false"
            });
            __alloyId8.add(__alloyId10);
        }
        $.__views.__alloyId0.views = views;
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.models = Alloy.createCollection("model");
    $.__views.index = Ti.UI.createWindow({
        backgroundColor: "white",
        id: "index"
    });
    $.__views.index && $.addTopLevelView($.__views.index);
    var __alloyId1 = [];
    $.__views.__alloyId0 = Ti.UI.createScrollableView({
        views: __alloyId1,
        showPagingControl: "true",
        height: "200",
        id: "__alloyId0"
    });
    $.__views.index.add($.__views.__alloyId0);
    var __alloyId11 = Alloy.Collections["$.models"] || $.models;
    __alloyId11.on("fetch destroy change add remove reset", __alloyId12);
    exports.destroy = function() {
        __alloyId11.off("fetch destroy change add remove reset", __alloyId12);
    };
    _.extend($, $.__views);
    $.index.open();
    var models = $.models;
    var model = Alloy.createModel("model", {
        name: "gus",
        image: "/gus.JPG"
    });
    models.push(model);
    model = Alloy.createModel("model", {
        name: "astros",
        image: "/astros.jpg"
    });
    models.add(model);
    model = Alloy.createModel("model", {
        name: "finterns",
        image: "/finterns.png"
    });
    models.add(model);
    model = Alloy.createModel("model", {
        name: "guns",
        image: "/guns.jpg"
    });
    models.add(model);
    model = Alloy.createModel("model", {
        name: "rockets",
        image: "/rockets.jpg"
    });
    models.add(model);
    $.index.addEventListener("close", function() {
        $.destroy();
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;