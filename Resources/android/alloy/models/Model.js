exports.definition = {
    config: {
        adapter: {
            type: "properties",
            collection_name: "model"
        }
    },
    extendModel: function(Model) {
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {});
        return Collection;
    }
};

var Alloy = require("alloy"), _ = require("alloy/underscore")._, model, collection;

model = Alloy.M("model", exports.definition, []);

collection = Alloy.C("model", exports.definition, model);

exports.Model = model;

exports.Collection = collection;